## Deutsch

[&darr; goto English](#english)
<h1>Motivation</h1>

Hintergrund dieses Projektes war eine Struts-Anwendung, in welcher die Eingabe von 
URL und E-Mail-Adressen validiert wurde. Das hatte gut funktioniert, bis die erste 
Umlaut-Domaine (internationalized domain name, IDN) ins Spiel kam. 
Die wird mit dem Struts-Validator nicht korrekt validiert. 
Eine URL wie [http://www.nürburg.de/](http://www.nürburg.de/) ist korrekt, wird aber als fehlerhaft betrachtet.

Im ersten Ansatz hatte ich die beiden betroffenen Struts-ValidatorKlassen 
(EmailValidator und URLValidator) überschrieben. Dabei wurde der Aufruf „getFieldValue“ 
überschrieben und der Wert mit Java-Methode „IDN.toASCII“ geändert an die Elternklasse 
weitergeleitet. Damit war auf Serverseite alles geregelt und meine Anwendung funktionierte wieder.

Ich hatte daraufhin das Problem auf der [Struts-Mailing-Liste](https://lists.apache.org/thread.html/9d7ac2dd3bc830cf9d82d0c01bbbfc3fe56b00eae0f55b65e05ba415%40%3Cuser.struts.apache.org%3E)
thematisiert
  
Meine Lösung war OK, es fehlte die Client-Lösung. Eine frei verfügbare JacaScript 
Lösung (unter Apache-Lizenz) hatte ich hierzu nicht gefunden. Der Vorschlag war, 
die Lösung per Ajax und DWR zu realisieren.

Leider funktionieren die Struts-Beispiele hierzu nicht (siehe Struts-App).
Ein Problem ist, dass das Struts-Framework eine gerenderte
HTML-Seite liefert und somit die Ausgabe des DWR-Servlets überschreibt.
Ich konnte dies umgehen, indem ich die HTTP-Response über einen Wrapper umgeleitet habe
(siehe meine DWRValidator-Klasse, struts2-dwr-plugin-2.5.26.jar wird daher nicht mehr benötigt).
  
Um DWR einzusetzen nutze ich als UI-Theme „simple“. Dort ist aktuell kein Client-Validator 
vorgesehen. Ich nutze im Beispiel ein neues Theme, welches ich „simpledwr“ genannt habe 
und welches von „simple“ erbt. Im Form-Tag muss das Theme angegeben werden, 
ferner der Schalter „validate“ auf „true“ gesetzt werden. Der Rest ist transparent.
  
Fehlermeldungen werden gesammelt vor dem Formular ausgegeben, dazu wird ein DIV-Tag mit 
der ID „dwr-errorlabel_standard_[FORM-NAME]" gefüllt. Das Tag ist im Template vorbelegt. 
An das Elterndokument eines Feldes wird ein DIV-Tag mit der ID 
„dwr-errorlabel_standard_[FORM-NAME]_[FIELD-NAME]“ angefügt.
  
Um Fehlermeldungen individuell zu gestalten, können DIV- oder SPAN-Tags wie oben angelegt 
werden. Es wird „standard“ mit „user“ ersetzt 
(„dwr-errorlabel_user_[FORM-NAME]" bzw. „dwr-errorlabel_user_[FORM-NAME]_[FIELD-NAME]“).
  
In SUBMIT-Felder baut das Template eine onClick-Methode ein, welche die Validerung 
per JavaScript startet.
  
Soll bei einem SUBMIT nicht geprüft werden, kann ein Attribut „data_validate=‘false‘" 
eingebaut werden. Dies kann sinnvoll sein, damit Reset-Felder nicht client-seitig geprüft 
werden.
  
Das Beispiel baut auf der neuesten Version von DWR auf. Die alte Version funktioniert auch, 
allerdings müssen dann Package-Definitionen geändert werden. Die alten Packages sind als 
Kommentare im Quellcode zu finden.
  
Im Beispiel sind die wichtigsten Fälle enthalten. Es kann mehr als ein Form je Seite 
vorkommen, allerdings sollten dann Unterschiedliche Form-Namen (Action) vorhanden sein.

Die Anwendung ist – je nach Browsereinstellung – in deutscher bzw. englischer Sprache.

Benötigt werden:  
- Das Beispiel baut auf „Struts-Blank-Example“ auf (struts2)
- zusätzliche JAR-Files: commons-logging-1.2.jar, dwr.jar
- struts2-dwr-plugin-2.5.26.jar – wird nicht benötigt, die entsprechende Klasse (DWRValidator ) wurde neu erstellt
- web.xml: Erweiterung um dwr-Servlet
- dwr.xml: neu im WEB-INF-Ordner
- struts.xml: simple-Theme auf Default setzten

<h2>Anpassungen</h2>

1. web.xml
```
    <servlet>
        <servlet-name>dwr</servlet-name>
        <!-- <servlet-class>uk.ltd.getahead.dwr.DWRServlet</servlet-class> -->
        <servlet-class>org.directwebremoting.servlet.DwrServlet</servlet-class>
        <init-param>
            <param-name>debug</param-name>
            <param-value>true</param-value> <!-- not in production -->
        </init-param>
        <init-param>
            <param-name>jsonpEnabled</param-name>
            <param-value>false</param-value> <!-- standard -->
        </init-param>
    </servlet>

    <servlet-mapping>
        <servlet-name>dwr</servlet-name>
        <url-pattern>/dwr/*</url-pattern>
    </servlet-mapping>
```

2. dwr.xml
```
<?xml version="1.0" encoding="UTF-8"?>
<!--
<!DOCTYPE dwr PUBLIC
  "-//GetAhead Limited//DTD Direct Web Remoting 1.0//EN"
  "http://www.getahead.ltd.uk/dwr/dwr10.dtd">
-->
<!DOCTYPE dwr PUBLIC
  "-//GetAhead Limited//DTD Direct Web Remoting 3.0//EN"
  "http://getahead.org/dwr/dwr30.dtd">
<dwr>
  <allow>
    <create creator="new" javascript="validator">
      <param name="class" value="de.guenterpaul.struts.dwrexample.struts.validator.DWRValidator"/>
      <!-- <param name="class" value="org.apache.struts2.validators.DWRValidator"/> -->
    </create>
    <convert converter="bean" match="com.opensymphony.xwork2.ValidationAwareSupport"/>
  </allow>

  <signatures>
    <![CDATA[
        import java.util.Map;
        import com.dwrexample.DWRValidator;

        DWRValidator.doPost(String, String, Map<String, String>);
    ]]>
  </signatures>
</dwr>
```
  
3. struts.xml (optional)
```
 <constant name="struts.ui.theme" value="simple" />
```
  
**Mehr Informationen**

https://struts.apache.org/plugins/dwr/  
http://directwebremoting.org/dwr/  
  
Die Beispielimplementation ist hier:  
https://www.fentool.de/dwr/
  

<hr />

## English
[&uarr; deutsche Version](#deutsch)
<h1>Motivation</h1>

The background to this project was a Struts application in which the entry of URL 
and email addresses was validated. That worked well until the first internationalized 
domain name (IDN) came into play. This is not correctly validated with the Struts 
validator. An URL such as [http://www.nürburg.de/](http://www.nürburg.de/) is correct, but is considered 
to be incorrect.
  
In the first approach, I had overwritten the two affected Struts validator classes
(EmailValidator and URLValidator). The call "getFieldValue" was overwritten and 
the value changed to the parent class with the Java method "IDN.toASCII". 
So everything was regulated on the server side and my application worked.

I reported the problem on the [Struts mailing list](https://lists.apache.org/thread.html/9d7ac2dd3bc830cf9d82d0c01bbbfc3fe56b00eae0f55b65e05ba415%40%3Cuser.struts.apache.org%3E).

My solution was OK, the client solution was missing. I hadn't found a freely available 
JacaScript solution (under Apache license) for this. The suggestion was to implement 
the solution via Ajax and DWR.

Unfortunately, the Struts examples don't work for this (see Struts app). One problem 
is the Struts framework delivers a rendered HTM page 
and overwrites the output of the DWR servlet. I was able to get around this by redirecting 
the HTTP response through a wrapper (see my DWRValidator class, so we don't need struts2-dwr-plugin-2.5.26.jar).

Useing DWR, I used the “simple” UI theme. There is currently no client validator 
provided there. In my example I am using a new theme named "simpledwr" which 
inherits from "simple". The theme must be specified in the form tag and the "validate" 
attribute must set to "true". All other is transparent.

Error messages are collected in front of the form, a DIV tag is filled with the ID 
"dwr-errorlabel_standard_ [FORM-NAME]". This tag is preset in the template. 
A DIV tag with the ID "dwr-errorlabel_standard_[FORM-NAME]_[FIELD-NAME]" 
will be added by the template.

To customize error messages DIV or SPAN tags can be created as above. "Standard" is 
to replaced with "user" ("dwr-errorlabel_user_[FORM-NAME]" or 
"dwr-errorlabel_user_[FORM-NAME]_[FIELD-NAME]").

In SUBMIT fields the template builds an onClick method starting the validation script
via JavaScript.

If a SUBMIT has not to use validation, you can set the attribute 
"data_validate=‘false‘". 
This can be useful for reset fields they are not to check by the client-validation.

The example use the latest version of DWR. The old version also works  
by change the package name. The old package-names can be found as comment 
in the source code.

The example contains the most important cases. It's possible to use more than one form per page
by using different form names (different actions)

The application is - depending on the browser setting - in German or English (default).

Requirements

- The example is based on "Struts-Blank-Example" (struts2) 
- Additional JAR files: commons-logging-1.2.jar, dwr.jar 
- struts2-dwr-plugin-2.5.26.jar - is not required, the corresponding class (DWRValidator) was newly created 
- web.xml: extension by dwr servlet 
- dwr.xml: new in the WEB-INF folder 
- struts.xml: set simple theme to default

<h2>Changes</h2>

1. web.xml
```
    <servlet>
        <servlet-name>dwr</servlet-name>
        <!-- <servlet-class>uk.ltd.getahead.dwr.DWRServlet</servlet-class> -->
        <servlet-class>org.directwebremoting.servlet.DwrServlet</servlet-class>
        <init-param>
            <param-name>debug</param-name>
            <param-value>true</param-value> <!-- not in production -->
        </init-param>
        <init-param>
            <param-name>jsonpEnabled</param-name>
            <param-value>false</param-value> <!-- standard -->
        </init-param>
    </servlet>

    <servlet-mapping>
        <servlet-name>dwr</servlet-name>
        <url-pattern>/dwr/*</url-pattern>
    </servlet-mapping>
```

2. dwr.xml
```
<?xml version="1.0" encoding="UTF-8"?>
<!--
<!DOCTYPE dwr PUBLIC
  "-//GetAhead Limited//DTD Direct Web Remoting 1.0//EN"
  "http://www.getahead.ltd.uk/dwr/dwr10.dtd">
-->
<!DOCTYPE dwr PUBLIC
  "-//GetAhead Limited//DTD Direct Web Remoting 3.0//EN"
  "http://getahead.org/dwr/dwr30.dtd">
<dwr>
  <allow>
    <create creator="new" javascript="validator">
      <param name="class" value="de.guenterpaul.struts.dwrexample.struts.validator.DWRValidator"/>
      <!-- <param name="class" value="org.apache.struts2.validators.DWRValidator"/> -->
    </create>
    <convert converter="bean" match="com.opensymphony.xwork2.ValidationAwareSupport"/>
  </allow>

  <signatures>
    <![CDATA[
        import java.util.Map;
        import com.dwrexample.DWRValidator;

        DWRValidator.doPost(String, String, Map<String, String>);
    ]]>
  </signatures>
</dwr>
```

3. struts.xml (optional)
```
 <constant name="struts.ui.theme" value="simple" />
```

**More Information**

https://struts.apache.org/plugins/dwr/  
http://directwebremoting.org/dwr/
  
You can found an example here:  
https://www.fentool.de/dwr/  
