/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

if (!SimpleDwrValidation)
{
  var SimpleDwrValidation = {
    getErrorClassName : function () { return "errorMessage";  },
    getPreError : function () { return "dwr-errorlabel_";  },
    getStandardErrorFormId : function(formname) { return SimpleDwrValidation.getPreError() + "standard_" + formname; },
    getStandardFielFormId : function(formname, fieldname) { return SimpleDwrValidation.getStandardErrorFormId(formname) + "_" + fieldname; },
    getUserErrorFormId : function (formname) { return SimpleDwrValidation.getPreError() + "user_" + formname;  },
    getUserErrorFieldId : function (formname, fieldname) { return SimpleDwrValidation.getUserErrorFormId(formname) + "_" + fieldname;  },

    removeMessages: function (usedform, formName)
    {
      if (usedform)
      {
        var formelements = usedform.elements;
        if (formelements)
        {
          for (var i=0; i<formelements.length; i++)
          {
            var aktelement = formelements[i];
            var aktelementerror = document.getElementById(SimpleDwrValidation.getStandardFielFormId(formName, aktelement.name));
            if (aktelementerror)
            {
              aktelementerror.parentNode.removeChild(aktelementerror);
            }
            var aktuserelementerror = document.getElementById(SimpleDwrValidation.getUserErrorFieldId(formName, aktelement.name));
            if (aktuserelementerror)
            {
              aktuserelementerror.innerHTML = "";
            }
          }
        }

        var containerliste = document.getElementById(SimpleDwrValidation.getStandardErrorFormId(formName));
        if (containerliste)
        {
          containerliste.innerHTML = "";
        }
        var containeruserliste = document.getElementById(SimpleDwrValidation.getUserErrorFormId(formName));
        if (containeruserliste)
        {
          containeruserliste.innerHTML = "";
        }
      }
    },

    handleMessages: function (data, usedform, formName)
    {
      var founderror = false;
      if (data && usedform)
      {
        var indexresult = -1;
        var listresult = [];
        var datajson = JSON.parse(JSON.stringify(data));
        for (var messagekind in datajson) {
          for (var fieldname in datajson[messagekind]) {
            var name = fieldname;
            var fieldvalue = datajson[messagekind][fieldname];

            indexresult++;
            listresult[indexresult] = fieldvalue;

            var id = SimpleDwrValidation.findID(usedform, name);
            if (id)
            {
              var containerElement = document.getElementById(id);
              var userField = true;
              var errorFieldId = SimpleDwrValidation.getUserErrorFieldId(formName, containerElement.name);
              var container = document.getElementById(errorFieldId);
              if (!container)
              {
                errorFieldId = SimpleDwrValidation.getUserErrorFieldId(formName, containerElement.name);
                container = containerElement;
                userField = false;
              }
              if (container) {
                var validationResultdiv = document.createElement("div");
                validationResultdiv.setAttribute("id", errorFieldId);
                validationResultdiv.setAttribute("class", "errorMessage");
                var newContent = document.createTextNode(fieldvalue);
                validationResultdiv.appendChild(newContent);
                if (userField === true)
                {
                  container.appendChild(validationResultdiv)
                }
                else
                {
                  container.parentElement.appendChild(validationResultdiv);
                }
              }
            }
          }
        }

        var containerliste = document.getElementById(SimpleDwrValidation.getUserErrorFormId(formName));
        if (!containerliste)
        {
          containerliste = document.getElementById(SimpleDwrValidation.getStandardErrorFormId(formName));
        }
        if (containerliste) {
          if (indexresult >= 0) {
            var liste = document.createElement("ul");
            for (index = 0; index < listresult.length; ++index) {
              var lsZeile = listresult[index];
              var elementliste = document.createElement("li");
              var newContentListe = document.createTextNode(lsZeile);
              elementliste.appendChild(newContentListe);
              liste.appendChild(elementliste);
            }
            containerliste.append(liste);
          }
        }

        if (indexresult >= 0)
        {
          founderror = true;
        }
      }
      return founderror;
    },

    findID: function (usedform, fieldname)
    {
      if (usedform)
      {
        if (fieldname)
        {
          var formelements = usedform.elements;
          if (formelements)
          {
            for (var i=0; i<formelements.length; i++)
            {
              var aktelement = formelements[i];
              if (aktelement.name === fieldname)
              {
                return aktelement.id;
              }
            }
          }
        }
      }
      return "";
    },

    arraySerialize: function (form)
    {
      var formArry = [];
      var myArray = [];
      if (typeof form === 'object' && form.nodeName === "FORM")
      {
        var len = form.elements.length;
        for (var i=0; i<len; i++)
        {
          myArray = form.elements[i];
          if (myArray.name && !myArray.disabled && myArray.type !== 'file' && myArray.type !== 'reset' && myArray.type !== 'submit' && myArray.type !== 'button')
          {
            if (myArray.type === 'select-multiple')
            {
              var l = form.elements[i].options.length;
              for (j=0; j<l; j++)
              {
                if(myArray.options[j].selected)
                {
                  formArry[formArry.length] = { name: myArray.name, value: myArray.options[j].value };
                }
              }
            }
            else if ( (myArray.type !== 'checkbox' && myArray.type !== 'radio') || (myArray.checked) )
            {
              formArry[formArry.length] = { name: myArray.name, value: myArray.value };
            }
          }
        }
      }

      var postData = {};
      formArry.map(function (x)
      {
        postData[x.name] = x.value;
      });

      return postData;
    }

  };
}
