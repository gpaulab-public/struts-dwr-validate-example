<#--
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
-->

<#if ((parameters.validate!false == true) && (parameters.performValidation!false == true))>
<script type="text/javascript">
    var myFormId_${parameters.name} = null;
    var myFormName_${parameters.name} = null;
    var dwrValidateReply_${parameters.name} = function(mydata)
    {
        if (myFormId_${parameters.name})
        {
            var myform = document.getElementById(myFormId_${parameters.name});
            if (myform)
            {
                SimpleDwrValidation.removeMessages(myform, myFormName_${parameters.name});

                var founderror = SimpleDwrValidation.handleMessages(mydata, myform, myFormName_${parameters.name});

                if (founderror === false)
                {
                    myform.submit();
                }
            }
        }
    }

    function dwrFormValidation_${parameters.name}(clickelement)
    {
        if (dwr)
        {
            if (clickelement)
            {
                var myform = clickelement.form;
                if (myform) {
                    myFormId_${parameters.name} = myform.id;
                    myFormName_${parameters.name} = '${parameters.name}';
                    var click_element = document.getElementById(clickelement.id);
                    if (click_element) {
                        var datavalidate = click_element.getAttribute("data_validate");
                        if (datavalidate) {
                            if (datavalidate === 'false') {
                                return true;
                            }
                        }
                    }
                    validator.doPost('${parameters.namespace}',
                            '${parameters.name}',
                            SimpleDwrValidation.arraySerialize(myform),
                            dwrValidateReply_${parameters.name});
                    return false;
                }
            }
        }
        return true;
    }

</script>
</#if>