<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
  <head>
    <title><s:text name="showuser"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>

  <body>
    <h1><s:text name="title"/></h1>

    <h2><s:text name="showuser"/></h2>

    <div style="max-width: max-content;">
      <s:iterator value="listUserData" var="user" >
        <s:url action="edituser" var="linkedit">
          <s:param name="userid" >${user.id}</s:param>
        </s:url>
        <div style="border: 1px solid black; width: available; padding: 5px; margin: 5px">
          <b style="font-weight: bolder; font-size: large"><s:text name="user"/></b><br />
          <a href="${linkedit}" ><s:property value="firstname" /> <s:property value="familyname" /></a><br />
          <s:property value="email" /><br />
          <s:property value="url" /><br />
          <b style="font-weight: bold;"><s:text name="physicaladdress"/></b><br />
          <s:property value="physicaladdress.zipcode" /> <s:property value="physicaladdress.city" /><br />
          <s:property value="physicaladdress.street" /><br />
          <b style="font-weight: bold;"><s:text name="deliveryaddress"/></b><br />
          <s:property value="deliveryaddress.zipcode" /> <s:property value="deliveryaddress.city" /><br />
          <s:property value="deliveryaddress.street"/>
        </div>
        <br />
      </s:iterator>
    </div>
  </body>
</html>
