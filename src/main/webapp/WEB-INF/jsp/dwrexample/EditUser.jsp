<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
  <head>
    <title><s:text name="edituser"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .errorMessage
        {
            font-weight: bold;
            color: red;
        }
    </style>
  </head>
  <body>
    <div style="max-width: max-content;">
      <h1><s:text name="title"/></h1>

      <h2><s:text name="edituser"/></h2>

      <h3><s:text name="user"/>: <s:property value="containerUserdata.firstname"  /> <s:property value="containerUserdata.familyname" /></h3>

      <s:url action="startpage" var="linkstartpage" />
      <div>
        <a href="${linkstartpage}" ><s:text name="overview" /></a>
      </div>

      <div>
        <b style="font-weight: bolder; font-size: large"><s:text name="user"/></b><br />
        <s:form action="saveuser" theme="simpledwr" validate="true" >
          <s:hidden name="userid" />

          <div>
            <b style="font-weight: bold; font-size: medium"><s:text name="firstname"/></b><br />
            <s:textfield name="containerUserdata.firstname" /><br />
            <div id="dwr-errorlabel_user_saveuser_containerUserdata.firstname" class="errorMessage" >
              <s:iterator value="fieldErrors['containerUserdata.firstname']" var="fielderror" >
                <s:property /><br />
              </s:iterator>
            </div>
          </div>

          <div>
            <b style="font-weight: bold; font-size: medium"><s:text name="familyname"/></b><br />
            <s:textfield name="containerUserdata.familyname" /><br />
            <div id="dwr-errorlabel_user_saveuser_containerUserdata.familyname" class="errorMessage" >
              <s:iterator value="fieldErrors['containerUserdata.familyname']" var="fielderror" >
                <s:property /><br />
              </s:iterator>
            </div>
          </div>

          <div>
            <b style="font-weight: bold; font-size: medium"><s:text name="email"/></b><br />
            <s:textfield name="containerUserdata.email" /><br />
            <div id="dwr-errorlabel_user_saveuser_containerUserdata.email" class="errorMessage" >
              <s:iterator value="fieldErrors['containerUserdata.email']" var="fielderror" >
                <s:property /><br />
              </s:iterator>
            </div>
          </div>

          <div>
            <b style="font-weight: bold; font-size: medium"><s:text name="url"/></b><br />
            <s:textfield name="containerUserdata.url" /><br />
            <div id="dwr-errorlabel_user_saveuser_containerUserdata.url" class="errorMessage" >
              <s:iterator value="fieldErrors['containerUserdata.url']" var="fielderror" >
                <s:property /><br />
              </s:iterator>
            </div>
          </div>

          <div>
            <s:submit name="saveuser" key="save" />
            <s:submit name="resetuser" key="reset" data_validate="false" />
          </div>
        </s:form>
      </div>

      <div>
        <b style="font-weight: bold;"><s:text name="physicaladdress"/></b><br />
        <s:form action="savephysicaladdress" theme="simpledwr" validate="true" >
          <s:hidden name="userid" />

          <div>
            <b style="font-weight: bold; font-size: medium"><s:text name="zipcode"/></b><br />
            <s:textfield name="containerUserdata.physicaladdress.zipcode" /><br />
            <div id="dwr-errorlabel_user_saveuser_containerUserdata.physicaladdress.zipcode" class="errorMessage" >
              <s:iterator value="fieldErrors['containerUserdata.physicaladdress.zipcode']" var="fielderror" >
                <s:property /><br />
              </s:iterator>
            </div>
          </div>

          <div>
            <b style="font-weight: bold; font-size: medium"><s:text name="city"/></b><br />
            <s:textfield name="containerUserdata.physicaladdress.city" /><br />
            <div id="dwr-errorlabel_user_saveuser_containerUserdata.physicaladdress.city" class="errorMessage" >
              <s:iterator value="fieldErrors['containerUserdata.physicaladdress.city']" var="fielderror" >
                <s:property /><br />
              </s:iterator>
            </div>
          </div>

          <div>
            <b style="font-weight: bold; font-size: medium"><s:text name="street"/></b><br />
            <s:textfield name="containerUserdata.physicaladdress.street" /><br />
            <div id="dwr-errorlabel_user_saveuser_containerUserdata.physicaladdress.street" class="errorMessage" >
              <s:iterator value="fieldErrors['containerUserdata.physicaladdress.street']" var="fielderror" >
                <s:property /><br />
              </s:iterator>
            </div>
          </div>

          <div>
            <s:submit name="savephysicaladdress" key="save" />
            <s:submit name="resetphysicaladdress" key="reset" data_validate="false" />
          </div>
        </s:form>
      </div>

      <div>
        <b style="font-weight: bold;"><s:text name="deliveryaddress"/></b><br />
        <s:form action="savedeliveryaddress" theme="simpledwr" validate="true" >
          <s:hidden name="userid" />

          <div>
            <b style="font-weight: bold; font-size: medium"><s:text name="zipcode"/></b><br />
            <s:textfield name="containerUserdata.deliveryaddress.zipcode" /><br />
            <div id="dwr-errorlabel_user_saveuser_containerUserdata.deliveryaddress.zipcode" class="errorMessage" >
              <s:iterator value="fieldErrors['containerUserdata.deliveryaddress.zipcode']" var="fielderror" >
                <s:property /><br />
              </s:iterator>
            </div>
          </div>

          <div>
            <b style="font-weight: bold; font-size: medium"><s:text name="city"/></b><br />
            <s:textfield name="containerUserdata.deliveryaddress.city" /><br />
            <div id="dwr-errorlabel_user_saveuser_containerUserdata.deliveryaddress.city" class="errorMessage" >
              <s:iterator value="fieldErrors['containerUserdata.deliveryaddress.city']" var="fielderror" >
                <s:property /><br />
              </s:iterator>
            </div>
          </div>

          <div>
            <b style="font-weight: bold; font-size: medium"><s:text name="street"/></b><br />
            <s:textfield name="containerUserdata.deliveryaddress.street" /><br />
            <div id="dwr-errorlabel_user_saveuser_containerUserdata.deliveryaddress.street" class="errorMessage" >
              <s:iterator value="fieldErrors['containerUserdata.deliveryaddress.street']" var="fielderror" >
                <s:property /><br />
              </s:iterator>
            </div>
          </div>

          <div>
            <s:submit name="savedeliveryaddress" key="save" />
            <s:submit name="resetdeliveryaddress" key="reset" data_validate="false" />
          </div>
        </s:form>
      </div>

      <div>
        <a href="${linkstartpage}" ><s:text name="overview" /></a>
      </div>
    </div>
  </body>
</html>
