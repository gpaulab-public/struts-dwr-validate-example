package de.guenterpaul.struts.dwrexample.struts.validator;

import com.opensymphony.xwork2.ActionProxy;
import com.opensymphony.xwork2.ActionProxyFactory;
import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.interceptor.ValidationAware;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.*;
import org.directwebremoting.WebContextFactory;
//import uk.ltd.getahead.dwr.WebContextFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class DWRValidator
{
  @SuppressWarnings("unused")
  public ValidationAwareSupport doPost(String nameSpace, String actionName, Map<String, ?> paramsMap)
  {
    ValidationAwareSupport validationAwareSupport;

    if (LOGGER.isDebugEnabled()) LOGGER.debug("Namespace >" + nameSpace + "< ActionName >" + actionName + "< ParamMap: " + paramsMap);
    try
    {
      HttpServletRequest httpServletRequest = WebContextFactory.get().getHttpServletRequest();
      ServletContext servletContext = WebContextFactory.get().getServletContext();
      NotUsedHttpServletResponse responseWrapper = new NotUsedHttpServletResponse(WebContextFactory.get().getHttpServletResponse());

      HttpParameters.Builder requestParams = HttpParameters.create(httpServletRequest.getParameterMap());
      if (paramsMap != null)
      {
        requestParams = requestParams.withExtraParams(paramsMap);
      }
      @SuppressWarnings("unchecked")
      Map<String, Object> requestMap = new RequestMap(httpServletRequest);
      Map<String, Object> sessionMap = new SessionMap<>(httpServletRequest);
      @SuppressWarnings("unchecked")
      Map<String, Object> applicationMap = new ApplicationMap(servletContext);
      Dispatcher dispatcher = Dispatcher.getInstance();
      Map<String, Object> lContextMap = dispatcher.createContextMap(requestMap,
        requestParams.build(),
        sessionMap,
        applicationMap,
        httpServletRequest,
        responseWrapper);

      ActionProxyFactory actionProxyFactory = dispatcher.getContainer().getInstance(ActionProxyFactory.class);
      ActionProxy actionProxy = actionProxyFactory.createActionProxy(nameSpace, actionName, null, lContextMap, true, true);
      httpServletRequest.setAttribute(ServletActionContext.STRUTS_VALUESTACK_KEY, actionProxy.getInvocation().getStack());
      actionProxy.execute();
      Object action = actionProxy.getAction();

      if (action instanceof ValidationAware)
      {
        validationAwareSupport = new ValidationAwareSupport();

        ValidationAware validationAwareAction = (ValidationAware) action;
        validationAwareSupport.setActionErrors(validationAwareAction.getActionErrors());
        validationAwareSupport.setActionMessages(validationAwareAction.getActionMessages());
        validationAwareSupport.setFieldErrors(validationAwareAction.getFieldErrors());

        if (LOGGER.isInfoEnabled())
        {
          LOGGER.info("ValidationAware hasErrors: " + validationAwareSupport.hasErrors());
        }
      }
      else
      {
        validationAwareSupport = null;
      }
    }
    catch (Exception ex)
    {
      validationAwareSupport = null;
      LOGGER.error("Error while trying to validate: " + ex.getMessage(), ex);
    }

    return validationAwareSupport;
  }

  private final static class NotUsedHttpServletResponse extends HttpServletResponseWrapper
  {
    public NotUsedHttpServletResponse(HttpServletResponse response)
    {
      super(response);
      m_CharArrayWriter = new CharArrayWriter();
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException
    {
      if (isWriterCalled())
      {
        throw new IllegalStateException("getWriter already called");
      }
      setOutputStreamCalled(true);
      return super.getOutputStream();
    }

    @Override
    public PrintWriter getWriter()
    {
      if (m_PrintWriter != null)
      {
        return m_PrintWriter;
      }
      if (isOutputStreamCalled())
      {
        throw new IllegalStateException("OutputStream already called");
      }
      setGetWriterCalled(true);
      m_PrintWriter = new PrintWriter(m_CharArrayWriter);
      return m_PrintWriter;
    }

    @Override
    public String toString()
    {
      String lsOut = null;
      if (m_PrintWriter != null)
      {
        lsOut = m_CharArrayWriter.toString();
      }
      return lsOut;
    }

    private boolean isOutputStreamCalled()
    {
      return outputStreamCalled;
    }

    private void setOutputStreamCalled(boolean outputStreamCalled)
    {
      this.outputStreamCalled = outputStreamCalled;
    }

    private boolean isWriterCalled()
    {
      return writerCalled;
    }

    private void setGetWriterCalled(boolean writerCalled)
    {
      this.writerCalled = writerCalled;
    }

    private final CharArrayWriter m_CharArrayWriter;
    private PrintWriter m_PrintWriter;
    private boolean outputStreamCalled = false;
    private boolean writerCalled = false;
  }

  private final static Logger LOGGER = LogManager.getLogger(DWRValidator.class.getName());

}
