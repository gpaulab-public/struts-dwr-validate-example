/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.guenterpaul.struts.dwrexample.struts.action;

import de.guenterpaul.struts.dwrexample.service.UserData;
import de.guenterpaul.struts.dwrexample.modell.ContainerUserdata;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import java.util.*;


/**
 * <code>Set welcome message.</code>
 */
public class StartPageAction extends ActionSupport implements SessionAware
{

    public String startpage() throws Exception
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("StartPageAction startpage: " + this);
        }

        List<ContainerUserdata> containerUserdataList = getListUserData();
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("containerUserdataList: " + containerUserdataList);
        }

        return SUCCESS;
    }

    public String saveuser() throws Exception
    {
        if (LOGGER.isDebugEnabled()) LOGGER.debug("StartPageAction saveuser: " + this);
        setSaveuser("saveuser");

        return edituser();
    }
    public String savephysicaladdress() throws Exception
    {
        if (LOGGER.isDebugEnabled()) LOGGER.debug("StartPageAction savephysicaladdress: " + this);
        setSavephysicaladdress("savephysicaladdress");

        return edituser();
    }
    public String savedeliveryaddress() throws Exception
    {
        if (LOGGER.isDebugEnabled()) LOGGER.debug("StartPageAction savedeliveryaddress: " + this);
        setSavedeliveryaddress("savedeliveryaddress");

        return edituser();
    }

    public String edituser() throws Exception
    {
        if (LOGGER.isDebugEnabled()) LOGGER.debug("StartPageAction edituser: " + this);
        String navigation = ERROR;

        int userid = getUserid();
        if (userid >= 0)
        {
            List<ContainerUserdata> containerUserdataList = getListUserData();
            if (containerUserdataList != null)
            {
                ContainerUserdata containerUserdataOriginal = searchId(userid, containerUserdataList);
                if (containerUserdataOriginal != null)
                {
                    navigation = INPUT;
                    if (containerUserdata != null)
                    {
                        if ((saveuser != null) && (saveuser.length() > 0))
                        {
                            containerUserdataOriginal.setFirstname(containerUserdata.getFirstname());
                            containerUserdataOriginal.setFamilyname(containerUserdata.getFamilyname());
                            containerUserdataOriginal.setEmail(containerUserdata.getEmail());
                            containerUserdataOriginal.setUrl(containerUserdata.getUrl());
                            navigation = SUCCESS;
                        }
                        else if ((savephysicaladdress != null) && (savephysicaladdress.length() > 0))
                        {
                            containerUserdataOriginal.getPhysicaladdress().setCity(containerUserdata.getPhysicaladdress().getCity());
                            containerUserdataOriginal.getPhysicaladdress().setStreet(containerUserdata.getPhysicaladdress().getStreet());
                            containerUserdataOriginal.getPhysicaladdress().setZipcode(containerUserdata.getPhysicaladdress().getZipcode());
                            navigation = SUCCESS;
                        }
                        else if ((savedeliveryaddress != null) && (savedeliveryaddress.length() > 0))
                        {
                            containerUserdataOriginal.getDeliveryaddress().setCity(containerUserdata.getDeliveryaddress().getCity());
                            containerUserdataOriginal.getDeliveryaddress().setStreet(containerUserdata.getDeliveryaddress().getStreet());
                            containerUserdataOriginal.getDeliveryaddress().setZipcode(containerUserdata.getDeliveryaddress().getZipcode());
                            navigation = SUCCESS;
                        }
                    }
                    containerUserdata = containerUserdataOriginal;
                }
            }
        }
        else if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("userid: " + userid);
        }

        return navigation;
    }

    public List<ContainerUserdata> getListUserData()
    {
        List<ContainerUserdata> containerUserdataList = null;
        if (userSessionMap != null)
        {
            //noinspection unchecked
            containerUserdataList = (List<ContainerUserdata>) userSessionMap.get(KEY_LIST_USERDATA);
        }
        if (containerUserdataList == null)
        {
            containerUserdataList = userData.getListContainerUserdata();
            if (userSessionMap != null)
            {
                userSessionMap.put(KEY_LIST_USERDATA, containerUserdataList);
            }
        }
        return containerUserdataList;
    }

    @Override
    public void validate()
    {
        super.validate();
        if (LOGGER.isDebugEnabled()) LOGGER.debug("Start Validate errors: " + hasErrors());
        if (hasErrors())
        {
            if (LOGGER.isDebugEnabled()) LOGGER.debug("FieldErrors: " + getFieldErrors() +
                                                      " ActionErrors: " + getActionErrors() +
                                                      " ActionMessages: " + getActionMessages());
        }

        // no Errors by reset data
        checkResetData();
    }

    public int getUserid()
    {
        return userid;
    }

    public void setUserid(int userid)
    {
        this.userid = userid;
    }

    public ContainerUserdata getContainerUserdata()
    {
        return containerUserdata;
    }

    public void setContainerUserdata(ContainerUserdata containerUserdata)
    {
        this.containerUserdata = containerUserdata;
    }

    public String getSaveuser()
    {
        return saveuser;
    }

    public void setSaveuser(String saveuser)
    {
        this.saveuser = saveuser;
    }

    public String getSavephysicaladdress()
    {
        return savephysicaladdress;
    }

    public void setSavephysicaladdress(String savephysicaladdress)
    {
        this.savephysicaladdress = savephysicaladdress;
    }

    public String getSavedeliveryaddress()
    {
        return savedeliveryaddress;
    }

    public void setSavedeliveryaddress(String savedeliveryaddress)
    {
        this.savedeliveryaddress = savedeliveryaddress;
    }

    public String getResetuser()
    {
        return resetuser;
    }

    public void setResetuser(String resetuser)
    {
        this.resetuser = resetuser;
    }

    public String getResetphysicaladdress()
    {
        return resetphysicaladdress;
    }

    public void setResetphysicaladdress(String resetphysicaladdress)
    {
        this.resetphysicaladdress = resetphysicaladdress;
    }

    public String getResetdeliveryaddress()
    {
        return resetdeliveryaddress;
    }

    public void setResetdeliveryaddress(String resetdeliveryaddress)
    {
        this.resetdeliveryaddress = resetdeliveryaddress;
    }

    @Override
    public void setSession(Map<String, Object> pSessionMap)
    {
        userSessionMap = pSessionMap;
    }


    @Override
    public String toString()
    {
        return "StartPageAction{" + "userid=" + userid + ", saveuser='" + saveuser + '\'' + ", savephysicaladdress='" + savephysicaladdress + '\'' + ", savedeliveryaddress='" + savedeliveryaddress + '\'' + ", resetuser='" + resetuser + '\'' + ", resetphysicaladdress='" + resetphysicaladdress + '\'' + ", resetdeliveryaddress='" + resetdeliveryaddress + '\'' + ", containerUserdata=" + containerUserdata + ", userSessionMap=" + userSessionMap + '}';
    }

    private ContainerUserdata searchId(int id, List<ContainerUserdata> containerUserdataList)
    {
        for (ContainerUserdata containerUserdata : containerUserdataList)
        {
            if (containerUserdata.getId() == id)
            {
                return containerUserdata;
            }
        }

        return null;
    }

    private void checkResetData()
    {
        List<ContainerUserdata> containerUserdataList = userData.getListContainerUserdata();
        int userid = getUserid();
        if ( (userid >= 0) && (userid < containerUserdataList.size()) )
        {
            if ( (getResetuser().length() > 0) ||
                 (getResetphysicaladdress().length() > 0) ||
                 (getResetdeliveryaddress().length() > 0) )
            {
                if (LOGGER.isDebugEnabled()) LOGGER.debug("Reset data and errors");

                Map<String, List<String>> mapFieldError = new HashMap<>();
                setFieldErrors(mapFieldError);
                Collection<String> collectionActionErrors = new ArrayList<>();
                setActionErrors(collectionActionErrors);

                if (containerUserdata != null)
                {
                    ContainerUserdata containerUserdataoriginal = containerUserdataList.get(userid);
                    if (getResetuser().length() > 0)
                    {
                        containerUserdata.setFirstname(containerUserdataoriginal.getFirstname());
                        containerUserdata.setFamilyname(containerUserdataoriginal.getFamilyname());
                        containerUserdata.setEmail(containerUserdataoriginal.getEmail());
                        containerUserdata.setUrl(containerUserdataoriginal.getUrl());
                    }
                    else if (getResetphysicaladdress().length() > 0)
                    {
                        containerUserdata.getPhysicaladdress().setZipcode(containerUserdataoriginal.getPhysicaladdress().getZipcode());
                        containerUserdata.getPhysicaladdress().setCity(containerUserdataoriginal.getPhysicaladdress().getCity());
                        containerUserdata.getPhysicaladdress().setStreet(containerUserdataoriginal.getPhysicaladdress().getStreet());
                    }
                    else if (getResetdeliveryaddress().length() > 0)
                    {
                        containerUserdata.getDeliveryaddress().setZipcode(containerUserdataoriginal.getDeliveryaddress().getZipcode());
                        containerUserdata.getDeliveryaddress().setCity(containerUserdataoriginal.getDeliveryaddress().getCity());
                        containerUserdata.getDeliveryaddress().setStreet(containerUserdataoriginal.getDeliveryaddress().getStreet());
                    }
                }
            }
        }
    }

    private int userid = -1;
    private String saveuser = "";
    private String savephysicaladdress = "";
    private String savedeliveryaddress = "";
    private String resetuser = "";
    private String resetphysicaladdress = "";
    private String resetdeliveryaddress = "";

    private ContainerUserdata containerUserdata = null;

    private Map<String, Object> userSessionMap = null;
    private static final String KEY_LIST_USERDATA = "ListContainerUserdatakey";

    private final static UserData userData = UserData.getUserData();
    private final static Logger LOGGER = LogManager.getLogger(StartPageAction.class.getName());
}
