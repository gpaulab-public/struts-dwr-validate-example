package de.guenterpaul.struts.dwrexample.struts.validator;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.EmailValidator;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.net.IDN;
import java.util.Objects;

public class EmailIdnValidator extends EmailValidator
{
  @Override
  public Object getFieldValue(String name, Object object) throws ValidationException
  {
    Object fieldValue = super.getFieldValue(name, object);

    String fieldValueString = Objects.toString(fieldValue, "").trim();
    if (fieldValueString.length() > 0)
    {
      fieldValueString = IDN.toASCII(fieldValueString);
      fieldValue = fieldValueString;
    }

    if (LOGGER.isDebugEnabled()) LOGGER.debug("fieldValue >" + fieldValueString + "<");

    return fieldValue;
  }

  private final static Logger LOGGER = LogManager.getLogger(EmailIdnValidator.class.getName());

}