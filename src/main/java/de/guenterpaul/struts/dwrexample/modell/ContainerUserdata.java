package de.guenterpaul.struts.dwrexample.modell;

public class ContainerUserdata
{
  public ContainerUserdata() {}

  public int getId()
  {
    return id;
  }

  public void setId(int id)
  {
    this.id = id;
  }

  public String getFamilyname()
  {
    return familyname;
  }

  public void setFamilyname(String familyname)
  {
    this.familyname = familyname;
  }

  public String getFirstname()
  {
    return firstname;
  }

  public void setFirstname(String firstname)
  {
    this.firstname = firstname;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getUrl()
  {
    return url;
  }

  public void setUrl(String url)
  {
    this.url = url;
  }

  public ContainerAddressdata getPhysicaladdress()
  {
    return physicaladdress;
  }

  public void setPhysicaladdress(ContainerAddressdata physicaladdress)
  {
    this.physicaladdress = physicaladdress;
  }

  public ContainerAddressdata getDeliveryaddress()
  {
    return deliveryaddress;
  }

  public void setDeliveryaddress(ContainerAddressdata deliveryaddress)
  {
    this.deliveryaddress = deliveryaddress;
  }

  @Override
  public String toString()
  {
    return "ContainerUserdata{" + "id=" + id + ", familyname='" + familyname + '\'' + ", firstname='" + firstname + '\'' + ", email='" + email + '\'' + ", url='" + url + '\'' + ", physicaladdress=" + physicaladdress + ", deliveryaddress=" + deliveryaddress + '}';
  }

  private int id = -1;
  private String familyname = "";
  private String firstname = "";
  private String email = "";
  private String url = "";
  private ContainerAddressdata physicaladdress = new ContainerAddressdata();
  private ContainerAddressdata deliveryaddress = new ContainerAddressdata();
}
