package de.guenterpaul.struts.dwrexample.service;

import de.guenterpaul.struts.dwrexample.modell.ContainerAddressdata;
import de.guenterpaul.struts.dwrexample.modell.ContainerUserdata;

import java.util.ArrayList;
import java.util.List;

public class UserData
{
  private UserData() {}

  public static synchronized UserData getUserData()
  {
    if (userData == null)
    {
      userData = new UserData();
    }
    return userData;
  }

  public List<ContainerUserdata> getListContainerUserdata()
  {
    List<ContainerUserdata> listContainerUserdata = new ArrayList<>();

    ContainerUserdata containerUser = new ContainerUserdata();
    containerUser.setId(0);
    containerUser.setFamilyname("Meier");
    containerUser.setFirstname("Gerd");
    containerUser.setEmail("info@gerdmeier.de");
    containerUser.setUrl("http://www.gerdmeier.de");

    ContainerAddressdata containerAddressdata = new ContainerAddressdata();
    containerAddressdata.setCity("Bonn");
    containerAddressdata.setZipcode("12345");
    containerAddressdata.setStreet("Hauptstraße 42");
    containerUser.setPhysicaladdress(containerAddressdata);

    containerAddressdata = new ContainerAddressdata();
    containerAddressdata.setCity("Köln");
    containerAddressdata.setZipcode("23456");
    containerAddressdata.setStreet("Bonnstraße 42");
    containerUser.setDeliveryaddress(containerAddressdata);

    listContainerUserdata.add(containerUser);

    containerUser = new ContainerUserdata();
    containerUser.setId(1);
    containerUser.setFamilyname("Müller");
    containerUser.setFirstname("Willi");
    containerUser.setEmail("info@willimüller.de");
    containerUser.setUrl("http://www.willimüller.de");

    containerAddressdata = new ContainerAddressdata();
    containerAddressdata.setCity("Bonn");
    containerAddressdata.setZipcode("12345");
    containerAddressdata.setStreet("Hauptstraße 44");
    containerUser.setPhysicaladdress(containerAddressdata);

    containerAddressdata = new ContainerAddressdata();
    containerAddressdata.setCity("Köln");
    containerAddressdata.setZipcode("23456");
    containerAddressdata.setStreet("Bonnstraße 44");
    containerUser.setDeliveryaddress(containerAddressdata);

    listContainerUserdata.add(containerUser);

    return listContainerUserdata;
  }

  private static UserData userData = null;

  @Override
  public String toString()
  {
    return "UserData: " + getListContainerUserdata();
  }

}
